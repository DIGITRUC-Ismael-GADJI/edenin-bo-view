import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { HELPERS } from '../helpers';
import 'rxjs/Rx';
import {Observable} from "rxjs";

@Injectable()
export class ContenuService {
  headers:Headers = new Headers({'Content-Type': 'application/json'})
  constructor(private http: Http,  private router: Router) {}


  getContent(livre_id) {
    let url = HELPERS.server + 'livres/' + livre_id;
    return this.http.get(url)
          .map((response: Response) => response.json())
          .catch((error: Response) => Observable.throw(error.json()))
  }



  addContent(data) {
    var body = JSON.stringify(data);
    var url = HELPERS.server + 'contenus'
    return this.http.post(url, body, {headers: this.headers})
                .map((response: Response) => response.json())
                .catch((error: Response) => Observable.throw(error.json()))
  }

  getSpecificContent(id) {
    var url = HELPERS.server + 'contenus/' + id;
    return this.http.get(url)
              .map((response: Response) => response.json())
              .catch((error: Response) => Observable.throw(error.json()))
  }


  addTextContent(data) {
    var body = JSON.stringify(data);
    var url = HELPERS.server + 'textes'
    return this.http.post(url, body, {headers: this.headers})
                .map((response: Response) => response.json())
                .catch((error: Response) => Observable.throw(error.json()))
  }

  addImageContent(data) {
    var body = JSON.stringify(data);
    var url = HELPERS.server + 'images'
    return this.http.post(url, body, {headers: this.headers})
                .map((response: Response) => response.json())
                .catch((error: Response) => Observable.throw(error.json()))
  }


  updateImageContent(data, id){
    var url = HELPERS.server + 'images/' + id;
    var body = JSON.stringify(data);
    return this.http.patch(url, body, {headers: this.headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
  }


  updateTextContent(data, id){
    var url = HELPERS.server + 'textes/' + id;
    var body = JSON.stringify(data);
    return this.http.patch(url, body, {headers: this.headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
  }


  deleteTexte(id) {
    let url = HELPERS.server + 'textes/' + id
    return this.http.delete(url, {headers: this.headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
   }

   deleteImage(id) {
    let url = HELPERS.server + 'images/' + id
    return this.http.delete(url, {headers: this.headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
   }

}
