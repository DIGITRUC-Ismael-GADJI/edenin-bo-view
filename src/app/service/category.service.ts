import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { HELPERS } from '../helpers';
import 'rxjs/Rx';
import {Observable} from "rxjs";

@Injectable()
export class CategoryService {
  header = new Headers({'Content-Type': 'application/json'})

  constructor(private http: Http,  private router: Router) {
    /**
       * if(this.cookieService.get('token') != undefined){
        this.token = this.cookieService.get('token');
        this.user = this.cookieService.getObject('user');
      }else{
        this.cookieService.removeAll();
        this.router.navigate(['/login']);
      }
     */
   }


   getCategories(){
     let url = HELPERS.server + 'categories'
     return this.http.get(url)
          .map((response: Response) => response.json())
          .catch((error: Response) => Observable.throw(error.json()))
   }


   addCategory(data) {
     var body = JSON.stringify(data);
     let url = HELPERS.server + 'categories';
     return this.http.post(url, body, {headers: this.header})
                .map((response: Response) => response.json())
                .catch((error: Response) => Observable.throw(error.json()))
   }


   deleteCategory(id){
    let url = HELPERS.server + 'categories/' + id
    return this.http.delete(url, {headers: this.header})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
  }

  updateCategory(data) {
    var body = JSON.stringify(data);
    let url = HELPERS.server + 'categories/' + data.id
      return this.http.patch(url, body, {headers: this.header})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
  }

}
