import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HELPERS } from '../helpers';
import 'rxjs/Rx';
import {Observable} from "rxjs";
import { Router } from '@angular/router';
@Injectable()
export class VideoService {

  headers:Headers = new Headers({'Content-Type': 'application/json'})
  constructor(private http: Http,  private router: Router) {
    /**
       * if(this.cookieService.get('token') != undefined){
        this.token = this.cookieService.get('token');
        this.user = this.cookieService.getObject('user');
      }else{
        this.cookieService.removeAll();
        this.router.navigate(['/login']);
      }
     */
   }

   getVideos(){
    let url = HELPERS.server + 'videos';
    return this.http.get(url)
         .map((response: Response) => response.json())
         .catch((error: Response) => Observable.throw(error.json()))
  }

  addVideo(data){
    var body = JSON.stringify(data);
    let url  = HELPERS.server + 'videos';
    return this.http.post(url, body, {headers: this.headers})
             .map((response: Response) => response.json())
             .catch((error: Response) => Observable.throw(error.json()))
  }

  updateVideo(data, id) {
    var body = JSON.stringify(data);
    let url = HELPERS.server + 'videos/' + id;
    return this.http.put(url, body, {headers: this.headers})
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error.json()))
  }


  deleteVideo(id) {
    let url = HELPERS.server + 'videos/' + id;
    return this.http.delete(url, {headers: this.headers})
          .map((response: Response) => response.json())
          .catch((error: Response) => Observable.throw(error.json())) 
  }

}
