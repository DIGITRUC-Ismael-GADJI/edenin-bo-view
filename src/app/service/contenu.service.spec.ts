import { TestBed, inject } from '@angular/core/testing';

import { ContenuService } from './contenu.service';

describe('ContenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContenuService]
    });
  });

  it('should be created', inject([ContenuService], (service: ContenuService) => {
    expect(service).toBeTruthy();
  }));
});
