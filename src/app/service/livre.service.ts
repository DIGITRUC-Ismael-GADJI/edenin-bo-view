import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers } from '@angular/http';
import { HELPERS } from '../helpers';
import 'rxjs/Rx';
import {Observable} from "rxjs";

@Injectable()
export class LivreService {
  headers:Headers = new Headers({'Content-Type': 'application/json'})
  constructor(private http: Http,  private router: Router) {
    /**
       * if(this.cookieService.get('token') != undefined){
        this.token = this.cookieService.get('token');
        this.user = this.cookieService.getObject('user');
      }else{
        this.cookieService.removeAll();
        this.router.navigate(['/login']);
      }
     */
   }


   getLivres(){
     let url = HELPERS.server + 'livres';
     return this.http.get(url)
          .map((response: Response) => response.json())
          .catch((error: Response) => Observable.throw(error.json()))
   }


   addBook(data){
     var body = JSON.stringify(data);
     let url  = HELPERS.server + 'livres';
     return this.http.post(url, body, {headers: this.headers})
              .map((response: Response) => response.json())
              .catch((error: Response) => Observable.throw(error.json()))
   }


   archiveBook(id) {
    let url = HELPERS.server + 'livres/' + id
    return this.http.delete(url, {headers: this.headers})
        .map((response: Response) => response.json())
        .catch((error: Response) => Observable.throw(error.json()))
   }

}
