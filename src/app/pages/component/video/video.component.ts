import { Component, OnInit } from '@angular/core';
import { VideoService } from '../../../service/video.service';
import { HELPERS } from '../../../helpers';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  public videos = []
  public model = {
    description: '',
    url: '',
    titre: '',
    categories: [],
    id: 1,
    allCategories: []
  }
  public listeCategories = []
  public tinymcekey:string = HELPERS.tinymceKey;
  public isLoading:boolean = false;
  constructor(private videoService: VideoService, private toastr: ToastrService) { }

  ngOnInit() {
    this.videoService.getVideos()
      .subscribe(
        response => {
            this.videos = response.videos;
            console.log(this.videos);
            response.categories.forEach(element => {
              this.listeCategories.push({
                value: element.id,
                label: element.nom
              })
            });
        },
        error => console.log(error)
      )
  }

  onSubmit(form) {
    if(form.valid) {
      this.isLoading = true;
      this.videoService.addVideo(this.model)
        .subscribe(
          response => {
            this.isLoading = false;
            this.toastr.success(response.message);
            $("#addVideoModal .close").click()
            this.ngOnInit();
          },
          error => {
            this.toastr.success(error.message);
            this.isLoading = false
          }
        )
    } 
  }

  prepareData(video) {
    this.model = {
      description: video.description,
      url: video.url,
      titre: video.titre,
      categories: [],
      id: video.id,
      allCategories:  video.categories
    }
  }

  updateVideo(form) {
    if(form.valid){
      this.isLoading = true;
      this.videoService.updateVideo(this.model, this.model.id)
        .subscribe(
          response => {
            this.isLoading = false;
            this.toastr.success(response.message);
            $("#updateVideoModal .close").click()
            this.ngOnInit();
          },
          error => {
            this.isLoading = false
            this.toastr.success(error.message);
          }
        )
    }
  }

  deleteVideo(video) {
    Swal.fire({
      title: 'Suppression de cette video',
      text: 'Une fois supprimé, cette video ne sera plus disponible',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.videoService.deleteVideo(video.id)
          .subscribe(
            response => {
              this.toastr.success(response.message);
              this.ngOnInit();
            },
            error => {
              this.toastr.success(error.message);
            }
          )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annuler',
          'Suppression annulée',
          'error'
        )
      }
    })
  }


}
