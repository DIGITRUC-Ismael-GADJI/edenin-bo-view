import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoComponent } from './video.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { DataTableModule } from 'angular2-datatable';
import { SelectModule } from 'angular2-select';
import { EditorModule } from '@tinymce/tinymce-angular';
import { VideoService } from '../../../service/video.service';

const routes: Routes = [{
  path: '',
	data: {
      title: 'Liste des videos',
      urls: [{title: 'Dashboard', url: '/'}, {title: 'Gestion des videos'}]
  },
	component: VideoComponent
}]

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    DataTableModule,
    SelectModule,
    EditorModule
  ],
  declarations: [VideoComponent],
  providers: [VideoService]
})
export class VideoModule { }
