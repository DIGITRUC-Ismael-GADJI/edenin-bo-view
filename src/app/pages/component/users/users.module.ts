import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [{
  path: '',
	data: {
      title: 'Catégorie',
      urls: [{title: 'Dashboard', url: '/'},{title: 'Gestion des utilisateurs'},{title: 'Utilisateurs'}]
    },
	component: UsersComponent
}]

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
