import { Component, OnInit } from '@angular/core';
import { ContenuService } from '../../../service/contenu.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HELPERS } from '../../../helpers';

@Component({
  selector: 'app-contenu',
  templateUrl: './contenu.component.html',
  styleUrls: ['./contenu.component.css']
})
export class ContenuComponent implements OnInit {

  contenus:any[] = []
  show:boolean = false;
  livre:any = {}
  livre_id:String = ""
  images:any[] = []

  langues:any[] = []
  dispositions:any[] = []

  modelContent:any = {
    position: 0,
    langue_id: "francais",
    disposition_id: "Image",
    livre_id: 1
  }
  public isLoading:boolean = false;
  public content:any = {}
  public model:any = {
    picture: '',
    position: 0
  }


  modelTexte:string = ''

  tinymcekey:string = HELPERS.tinymceKey;
  public isCollapsed = false;
  constructor(private contenuService: ContenuService, private route: ActivatedRoute,
      private toastr: ToastrService) { 
    this.livre_id = this.route.snapshot.paramMap.get('id')
    this.modelContent.livre_id = this.livre_id;
  }

  ngOnInit() {
    this.contenuService.getContent(this.livre_id)
      .subscribe(
        response => {
          this.livre = response.livre;
          this.contenus = response.contenus
          this.langues = response.langues;
          this.dispositions = response.dispositions;
          this.images = response.allImages
        },
        error => console.log(error)
      )
  }


  addContent(form){
    this.contenuService.addContent(this.modelContent)
      .subscribe(
        response => {
          this.contenus = response.contenus;
          $("#addContentInBook .close").click()
          this.toastr.success(response.message);

        },
        error => console.log(error)
      )
  }


  addText(form){
    var body = {
      langue_id: this.content.langue.id,
      contenu_id: this.content.id,
      description: this.modelTexte,
      position: this.model.position
    };
    this.contenuService.addTextContent(body)
      .subscribe(
        response => {
          this.modelTexte = ''
          $("#addText .close").click()
          this.toastr.success(response.message);
        },
        error => console.log(error)
      )
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.model.picture = btoa(binaryString);
    this.isLoading = false;
   }

  handleFileSelect(evt){
    this.isLoading = true;
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }
  

  addImage(form) {
    if(form.valid){
      var body = {
        langue_id: this.content.langue.id,
        contenu_id: this.content.id,
        picture: this.model.picture,
        position: this.model.position
      };
      this.isLoading = true;
      this.contenuService.addImageContent(body)
        .subscribe(
          response => {
            this.isLoading = false;
            $("#addImage .close").click()
            this.toastr.success(response.message);
          },
          error => console.log(error)
        )
      }
    }

}
