import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ContenuComponent } from './contenu.component';
import { ContenuService } from '../../../service/contenu.service';
import { EditorModule } from '@tinymce/tinymce-angular';

const routes: Routes = [{
  path: '',
	data: {
      title: 'Contenu du livre',
      urls: [{title: 'Dashboard', url: '/'}, {title: 'Gestion des livres', url: '/books'}, {title: 'Contenu'}]
  },
	component: ContenuComponent
}]

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FormsModule,
    RouterModule.forChild(routes),
    EditorModule
  ],
  declarations: [ContenuComponent],
  providers: [ContenuService]
})
export class ContenuModule { }
