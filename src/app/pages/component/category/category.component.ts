import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../service/category.service';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  model: any = {
    nom: '',
    etat: 1,
    id: 0,
    element: 0
  }
  isLoading:boolean = false;

  categories:any[] = []
  constructor(private categoryService: CategoryService, private toastr: ToastrService) { }

  ngOnInit() {
    this.categoryService.getCategories()
      .subscribe(
        response => {
          response.categories.forEach(element => {
            this.categories.push({
              nom: element.nom,
              etat: element.etat,
              element: Math.random(),
              id: element.id
            })
          });
        },
        error => {
          console.log(error.message)
        }
      )
  }

  onSubmit(form){
    this.isLoading = true;
    this.categoryService.addCategory(this.model)
      .subscribe(
        response => {
          this.isLoading = false;
          form.reset()
          this.toastr.success(response.message)
          this.categories.push({
            nom: response.categories.category.nom,
            element: response.categories.element,
            id: response.categories.category.id,
            etat: response.categories.category.etat,
          })
        },
        error => console.log(error.message)
      )
  }

  prepareData(category) {
    this.model = category;
    console.log(this.model);
  }

  sendUpdateCategory(form) {
    this.isLoading = true;
    this.categoryService.updateCategory(this.model)
      .subscribe(
        response => {
          this.isLoading = false;
          $("#updateCategoryModal .close").click()
          this.toastr.success(response.message)
        },
        error => console.log(error)
      )
  }

  deleteCategory(category){
    Swal.fire({
      title: 'Suppression de' + category.nom,
      text: 'Une fois supprimé, cette catégorie ne pourra plus être utilisée',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.categoryService.deleteCategory(category.id)
          .subscribe(
            response => {
              Swal.fire(
                'Supprimée',
                'La catégorie a été supprimée',
                'success'
              )
              this.categories = []
              response.categories.forEach(element => {
                this.categories.push({
                  nom: element.nom,
                  etat: element.etat,
                  element: Math.random(),
                  id: element.id
                })
              });
            },
            error => {
              console.log(error)
            }
          )
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annuler',
          'Suppression annulée',
          'error'
        )
      }
    })
  }
}
