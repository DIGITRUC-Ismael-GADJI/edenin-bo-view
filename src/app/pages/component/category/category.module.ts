import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { CategoryService } from '../../../service/category.service';

const routes: Routes = [{
  path: '',
	data: {
      title: 'Catégorie',
      urls: [{title: 'Dashboard', url: '/'},{title: 'Gestion des données'},{title: 'Catégorie'}]
    },
	component: CategoryComponent
}]

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    DataTableModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes)
  ],
  declarations: [CategoryComponent],
  providers: [CategoryService]
})
export class CategoryModule { }
