import { Component, OnInit } from '@angular/core';
import { LivreService } from '../../../service/livre.service';
import { HELPERS } from '../../../helpers';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  isLoading:boolean = false;
  tinymcekey:string = HELPERS.tinymceKey; 
  illustrateurs:any[] = []
  categories:any[] = []
  auteurs:any[] = []
  langues:any[] = []
  livres:any[] = []
  model:any = {
    titre: '',
    etat: "0",
    picture: '',
    categories: [],
    description: '',
    auteurs: [],
    langue: 1,
    illustrateurs: []
  }
  constructor(private livreService: LivreService, private toastr: ToastrService) { }

  ngOnInit() {
    this.livreService.getLivres()
      .subscribe(
        response => {
          this.livres = response.adminBooks;
          this.formatList(response.categories, this.categories);
          this.formatList(response.auteurs, this.auteurs);
          this.formatList(response.illustrateurs, this.illustrateurs);
          this.formatList(response.langues, this.langues);
        },
        error => console.log(error)
      )
  }



  formatList(data, list){
    data.forEach(element => {
      list.push({
        label: element.nom,
        value: element.id
      })
    });
  }


  onSubmit(form) {
    this.isLoading = true;
    this.livreService.addBook(this.model)
      .subscribe(
        response => {
          this.toastr.success(response.message)
          $("#addBookModal .close").click()
          this.isLoading = false;
        },
        error => console.log(error)
      )
  }



  handleFileSelect(evt){
    this.isLoading = true;
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }



_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
   this.model.picture = btoa(binaryString);
   this.isLoading = false;
  }



  prepareData(livre) {}

  deleteBook(livre) {
    Swal.fire({
      title: 'Suppression de' + livre.nom,
      text: 'Une fois supprimé, ce livre ne sera plus disponible',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.livreService.archiveBook(livre.id)
          .subscribe(
            response => {
              Swal.fire(
                'Supprimée',
                'Le livre a été supprimée',
                'success'
              )
              this.livres = []
              this.livres = response.adminBooks;
            },
            error => {
              console.log(error)
            }
          )
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annuler',
          'Suppression annulée',
          'error'
        )
      }
    })
  }

}
