import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {SelectModule} from "angular2-select";
import { DataTableModule } from 'angular2-datatable';
import { LivreService } from '../../../service/livre.service';
import { FormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';

const routes: Routes = [{
  path: '',
	data: {
      title: 'Livres',
      urls: [{title: 'Dashboard', url: '/'}, {title: 'Liste des livres'}]
    },
	component: BooksComponent
}]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    DataTableModule,
    SelectModule,
    EditorModule
  ],
  declarations: [BooksComponent],
  providers: [LivreService]
})
export class BooksModule { }
