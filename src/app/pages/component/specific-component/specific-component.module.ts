import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecificComponentComponent } from './specific-component.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContenuService } from '../../../service/contenu.service';
import { FormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';


const routes: Routes = [{
  path: '',
	data: {
      title: 'Contenu du livre',
      urls: [{title: 'Dashboard', url: '/'}, {title: 'Gestion des livres', url: '/books'}, {title: 'Liste des éléments'}]
  },
	component: SpecificComponentComponent
}]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    EditorModule
  ],
  declarations: [SpecificComponentComponent],
  providers: [ContenuService]
})
export class SpecificComponentModule { }
