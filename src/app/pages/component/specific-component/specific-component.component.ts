import { Component, OnInit } from '@angular/core';
import { ContenuService } from '../../../service/contenu.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { HELPERS } from '../../../helpers';

@Component({
  selector: 'app-specific-component',
  templateUrl: './specific-component.component.html',
  styleUrls: ['./specific-component.component.css']
})
export class SpecificComponentComponent implements OnInit {

  public contenu_id:string = '';
  public elements:any = {
    textes: [],
    images: []
  };

  public lastImage = {
    url: '',
    position: 0,
    id: 1,
    image: ''
  }
  public lastTexte = {
    description: '',
    position: 0,
    id: 1,
  } 
  public model:any = {
    picture: '',
    position: 0
  }
  public isLoading:boolean = false;
  tinymcekey:string = HELPERS.tinymceKey;
  constructor(private contenuService: ContenuService, private route: ActivatedRoute,
    private toastr: ToastrService) {
      this.contenu_id = this.route.snapshot.paramMap.get('id-contenu')
    }

  ngOnInit() {
    console.log('reload');
    this.contenuService.getSpecificContent(this.contenu_id)
      .subscribe(
        response => {
          this.elements = response
        },
        error => console.log(error)
      )
  }


  deleteTexte(texte) {
    Swal.fire({
      title : "Suppression d'un élement",
      text  : 'Une fois supprimé, ce texte ne sera plus disponible',
      type  : 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.contenuService.deleteTexte(texte.id)
          .subscribe(
            response => {
              this.ngOnInit()
              this.toastr.success(response.message);
            },
            error => {
              this.toastr.success(error.message);
            }
          )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annuler',
          'Suppression annulée',
          'error'
        )
      }
    })
  }


  deleteImage(image) {
    Swal.fire({
      title : "Suppression d'un élement",
      text  : 'Une fois supprimée, cette image ne sera plus disponible',
      type  : 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, supprimer!',
      cancelButtonText: 'Annuler'
    }).then((result) => {
      if (result.value) {
        this.contenuService.deleteImage(image.id)
          .subscribe(
            response => {
              this.ngOnInit()
              this.toastr.success(response.message);
            },
            error => {
              this.toastr.success(error.message);
            }
          )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Annuler',
          'Suppression annulée',
          'error'
        )
      }
    })
  }

  prepareDate(image){
    this.lastImage.position = image.position;
    this.lastImage.url = image.url
    this.lastImage.id = image.id
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.model.picture = btoa(binaryString);
    this.isLoading = false;
   }

  handleFileSelect(evt){
    this.isLoading = true;
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  prepareTexte(texte){
    this.lastTexte.description = texte.description;
    this.lastTexte.position = texte.position;
    this.lastTexte.id =  texte.id;
  }

  updateImage(form){
    if(form.valid) {
      this.isLoading = true;
      this.lastImage.image = this.model.picture;
      this.contenuService.updateImageContent(this.lastImage, this.lastImage.id)
        .subscribe(
          response => {
            this.isLoading = false;
            $("#updateImageModal .close").click()
            this.ngOnInit()
            this.toastr.success(response.message);
          },
          error => {
            this.toastr.success(error.message);
          }
        )
    }
  }

  updateText(form){
    if(form.valid) {
     this.ngOnInit(); 
     this.contenuService.updateTextContent(this.lastTexte, this.lastTexte.id)
      .subscribe(
        response => {
            this.isLoading = false;
            $("#updateTexteModal .close").click()
            this.ngOnInit()
            this.toastr.success(response.message);
        },
        error => {
          this.toastr.success(error.message);
        }
      ) 
    }
  }

}
