import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificComponentComponent } from './specific-component.component';

describe('SpecificComponentComponent', () => {
  let component: SpecificComponentComponent;
  let fixture: ComponentFixture<SpecificComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
