import {CommonModule} from "@angular/common";
import {TableFilterPipe} from "./tableFilterPipe.pipe";
import {NgModule} from "@angular/core";
@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TableFilterPipe,
  ],
  exports: [
    TableFilterPipe,
  ]
})

export class TableFilterPipeModule {}
